#ifndef _BWConverter_h_
#define _BWConverter_h_

#include "DBFileConverter.h"

class BWConverter : public DBFileConverter
{

 public:
  void Convert(std::string input, std::string output);
  void MakeTestLUT(std::string input, std::string output);
  void MakeCoeFile(const std::string &outputdir);
  int  TestPattern(int dr, int dphi);

};

#endif //_BWConverter_h_
