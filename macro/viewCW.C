#include <TMath.h>
#include <TROOT.h>
#include <TFile.h>
#include <TH2.h>

void viewCW(){

  //  TFile* coinfile = TFile::Open("../rootfile/newCoinFile_plus8GeV.root", "READ");
  TFile* coinfile = TFile::Open("../rootfile/oldCoinFile_0025.root", "READ");
  TH2D* hist_CW;

  Int_t isForward;
  Int_t side;
  Int_t oct;
  Int_t mod;
  Int_t roi;
  Int_t pt;

  std::cout << "Select region (Endcap:0, Forward:1): " ;
  std::cin >> isForward;

  if(isForward == 0){
    std::cout << "Select side (C:0, A:1): " ;
    std::cin >> side;
    std::cout << "Select octant (0 - 7): " ;
    std::cin >> oct;
    std::cout << "Select module (0 - 5): " ;
    std::cin >> mod;
    std::cout << "Select RoI (0 - 147): " ;
    std::cin >> roi;
    hist_CW = dynamic_cast<TH2D*>(coinfile->Get(Form("hist_Endcap_%d_%d_%d_%d", side, oct, mod, roi)));

    TCanvas* c1 = new TCanvas("c1", "c1", 600, 1000);
    hist_CW->Draw("colz text");
    Int_t xlabel = -7;
    for(Int_t i=1; i<=15; i++){
      std::stringstream label;
      label << xlabel;
      hist_CW->GetXaxis()->SetBinLabel(i, label.str().c_str());
      xlabel++;
    }
   Int_t ylabel = -15;
    for(Int_t i=1; i<=31; i++){
      std::stringstream label;
      label << ylabel;
      hist_CW->GetYaxis()->SetBinLabel(i, label.str().c_str());
      ylabel++;
    }
    gStyle->SetTitleYOffset(3);
    gStyle->SetOptStat(0);

  }

  if(isForward == 1){
    std::cout << "Select side (C:0, A:1): " ;
    std::cin >> side;
    std::cout << "Select octant (0 - 7): " ;
    std::cin >> oct;
    std::cout << "Select module (0 - 5): " ;
    std::cin >> mod;
    std::cout << "Select RoI (0 - 63): " ;
    std::cin >> roi;

    hist_CW = dynamic_cast<TH2D*>(coinfile->Get(Form("hist_Forward_%d_%d_%d_%d", side, oct, mod, roi)));

    TCanvas* c1 = new TCanvas("c1", "c1", 600, 1000);
    hist_CW->Draw("colz text");
    Int_t xlabel = -7;
    for(Int_t i=1; i<=15; i++){
      std::stringstream label;
      label << xlabel;
      hist_CW->GetXaxis()->SetBinLabel(i, label.str().c_str());
      xlabel++;
    }
   Int_t ylabel = -15;
    for(Int_t i=1; i<=31; i++){
      std::stringstream label;
      label << ylabel;
      hist_CW->GetYaxis()->SetBinLabel(i, label.str().c_str());
      ylabel++;
    }    
    c1->SetGrid(1);
    gStyle->SetOptStat(0);
  }

  return;

}
