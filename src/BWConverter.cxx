#include <stdint.h>
#include <iomanip>
#include <fstream>
#include <sstream>
#include "BWConverter.h"

void BWConverter::Convert(std::string inputdir, std::string outputdir){
  std::string kSide[2] = {"c", "a"};
  std::string kModule[12] = {"0a", "1a", "3a", "4a", "6a", "7a", "2a", "5b", "8a", "2b", "5a", "8b"}; 
  int kRoI[2] = {148, 64};

  for(int side=0; side<2; side++){
    for(int oct=0; oct<8; oct++){
      for(int mod=0; mod<12; mod++){
	  std::stringstream ss_input;
	  ss_input << "cm_" << kSide[side] << oct+1 << kModule[mod] << ".db";
	  std::string inputname = inputdir + ss_input.str();
	  std::cout << inputname << std::endl;

	  std::ifstream inputfile;
	  inputfile.open((inputname).c_str());
	  if(inputfile.fail()) continue;
  
	  int address;
	  int roi;

	  int pt[19][4][4096] = {{{}}}; // pt[ssc][phi][address]
	  char NON;
	  int tmp;
	  int checkR;
	  int checkPhi;
	  int dr = 0;
	  int SSC = 0;
	  
	  std::string line;
	  while(getline(inputfile, line)){
	    std::istringstream ss_line(line);
	    if(line[0] == '#'){
	      ss_line >> NON >> std::dec >> roi >> checkR >> tmp 
		      >> checkPhi >> tmp;
	    }else{
	      SSC = (roi < 4) ? 0 : (roi + 4)/8;
	      int PhiHL;
	      int RHL;
	      int R_pos;
	      int Phi_pos;
	      if(checkPhi == -7){
		PhiHL = 1;
	      }else{
		PhiHL = 0;
	      }
	      if(checkR == -15){
		RHL = 1;
	      }else{
		RHL = 0;
	      }
	      R_pos = (roi >> 2) & 0x1;
	      Phi_pos = roi & 0x3;
	      for(int dphi = 0; dphi < 15; dphi++){
		address = dr + dphi*32 + RHL*512 + PhiHL*1024 + R_pos*2048;
		ss_line >> std::hex >> pt[SSC][Phi_pos][address];
	      }
	      if(dr == 30){
		dr = 0;
	      }else{
		dr++;
	      }
	    }
	  }
	  inputfile.close();
	  
	  std::stringstream ss_output;
	  ss_output << "sl_" << kSide[side] << oct+1 << kModule[mod] << "_0030_LUTconf_test.txt";
	  std::string outputname = outputdir + ss_output.str();
	  std::cout << "output:   " << outputname << std::endl;
	  std::ofstream outputfile((outputname).c_str());
	  int cnt_roi = 0;
	  int cnt_add0 = 0;
	  int cnt_add1 = 0;
	  
		for(int ssc=0;ssc<19;ssc++){
			for(int cnt=0;cnt<4;cnt++){
				for(int addr=0;addr<4096;addr+=3){
  	  	      		int data0 = pt[ssc][cnt][addr    ];
  	  	      		int data1 = pt[ssc][cnt][addr + 1];
  	  	      		int data2 = pt[ssc][cnt][addr + 2];
  	  	      		uint16_t tmp = (data2 << 10 ) | (data1 << 5) | data0;
  	  	      		outputfile << "1 : 0x" << std::hex << std::setfill('0') << std::setw(4) << tmp << std::endl;
	  	      		cnt_add0++;
				}
			}
		}
	  std::cerr << cnt_add0 << std::endl;
	  outputfile.close();
      }
    }
  }
  return;  
}

void BWConverter::MakeTestLUT(std::string inputdir, std::string outputdir){
    int kRoI[2] = {148, 64};
    
    int pt[19][4][4096] = {};

    for(int ssc=0;ssc<19;ssc++){
        for(int phi_pos=0;phi_pos<4;phi_pos++){
            int cnt = 0;
            int flg = 1;
            for(int addr=0;addr<4096;addr++){
                pt[ssc][phi_pos][addr] = cnt;
                cnt += flg;
                if(cnt == 0 || cnt == 31) flg *= -1;
                //pt[ssc][phi_pos][addr] = 0;
            }
        }
    }

    std::string outputname = outputdir + "test_LUT.txt";
    std::cout << "output:   " << outputname << std::endl;
    std::ofstream outputfile((outputname).c_str());
    int cnt_roi = 0;
    int cnt_add0 = 0;
    int cnt_add1 = 0;
    int pt_tot[19*4096*4] = {};

    for(int ssc=0;ssc<19;ssc++){
        for(int cnt=0;cnt<4;cnt++){
      		for(int addr=0;addr<4096;addr+=3){
          		int data0 = pt[ssc][cnt][addr    ];
          		int data1 = pt[ssc][cnt][addr + 1];
          		int data2 = pt[ssc][cnt][addr + 2];
          		uint16_t tmp = (data2 << 10 ) | (data1 << 5) | data0;
          		outputfile << "1 : 0x" << std::hex << std::setfill('0') << std::setw(4) << tmp << std::endl;
          		cnt_add0++;
            }
        }
    }
    std::cerr << cnt_add0 << std::endl;
    outputfile.close();
    return;  
}

void BWConverter::MakeCoeFile(const std::string &outputdir){
    const int LUT_depth = 4096;
    const int write_length = 10; // how many data in ONE line of .coe file
    int pt[4096] = {};

    for(int dr=0;dr<31;dr++){
        for(int dphi=0;dphi<15;dphi++){
            for(int RHL=0;RHL<2;RHL++){
                for(int PhiHL=0;PhiHL<2;PhiHL++){
                    for(int R_pos=0;R_pos<2;R_pos++){
        	            int address = dr + dphi*32 + RHL*512 + PhiHL*1024 + R_pos*2048;
                        pt[address] = TestPattern(dr, dphi);
                    }
                }
            }
        }
    }

    std::string filename = outputdir + "testLUT_BW.coe";
    std::ofstream ofs(filename.c_str());
    ofs << "; .coe file for BW LUT. " << std::endl;
    ofs << "; LUT depth = 4096, LUT width = 4bit" << std::endl;
    ofs << "memory_initialization_radix=16;" << std::endl;
    ofs << "memory_initialization_vector=" << std::endl;
    for(int addr=0;addr<LUT_depth;addr++){
        ofs << std::hex << pt[addr];
        if(addr != LUT_depth-1) ofs << "," << std::endl;
        else                  ofs << ";" << std::endl;
    }

    return;
}

int BWConverter::TestPattern(int dr, int dphi){
    if( (12 <= dr && dr <= 20 ) && (6 <= dphi && dphi <= 9 ) ) return 8;
    if( (4  <= dr && dr <= 27 ) && (3 <= dphi && dphi <= 12) ) return 4;
    return 2;
}
