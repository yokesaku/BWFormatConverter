#include "BWConverter.h"

int main(void){

  std::string inputdir = "/home/okesaku/software/DB_LUT/yoshida/DBFiles/BW/";
  std::string outputdir = "/home/okesaku/software/DB_LUT/yoshida/DBFiles/BW_address/";
  
  if(mkdir(outputdir.c_str(), 0777) == 0){
    std::cout << "[INFO] Directry " << outputdir 
	      << " is created" << std::endl;
  }else{
    std::cout << "[WARNING] Directry " << outputdir 
	      << " is NOT created" << std::endl;
  }
  
  BWConverter bw; 
  //bw.Convert(inputdir, outputdir);
  //bw.MakeTestLUT(inputdir, outputdir);
  bw.MakeCoeFile(outputdir);

  return 0;
}
